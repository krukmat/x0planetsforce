# Salesforce: X0PlanetsForce

This is a django rest framework api to allow Salesforce do searches in nasa exo planets database. The information displayed is based on findings done with the package LightKurve.
More information: https://docs.lightkurve.org/tutorials/index.html  

# Live demo here:

soon

# To deploy to heroku

```bash
$ git init
$ git add .
$ git commit -am"First commit"
$ heroku create x0planetsforce
$ git push heroku master
$ heroku config:set DJANGO_SETTINGS_MODULE=coronaapi.settings
$ heroku run python manage.py migrate
$ heroku run python manage.py createsuperuser
$ heroku ps:scale web=1
```



