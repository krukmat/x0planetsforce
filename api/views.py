import io

import lightkurve as lk
import numpy as np
from django.http import HttpResponse
from matplotlib import pylab
from pylab import *
from rest_framework.views import APIView

from .mixins import JSONResponseMixin


class PingView(APIView):

	def get(self, request):
		"""
		Just responds with Pong
		"""
		data = {'ping': 'pong'}

		return JSONResponseMixin(data)
	
class LightKurveView(APIView):
	def cast_int(self, request, parameter, default):
		try:
			return int(request.query_params.get(parameter))
		except:
			return default
	def get(self, request):	
		# TODO: Parametros: Planeta, Quarter, Period_start, Period_end, period_step,window_length,bin_size
		planet = request.query_params.get('planet') #"Kepler-10"
		quarter = self.cast_int(request, 'quarter', 1) #3
		window_length = self.cast_int(request, 'window_length', 301)
		bin_size = self.cast_int(request, 'bin_size', 10)
		mision=request.query_params.get('mision')
		period_start = request.query_params.get('period_start')
		period_end = request.query_params.get('period_end')
		period_step = request.query_params.get('period_step')
		tpf = lk.search_targetpixelfile(planet, quarter=quarter).download()
		lc = tpf.to_lightcurve(aperture_mask=tpf.pipeline_mask)
		flat, trend = lc.flatten(window_length=window_length, return_trend=True)
		periodogram = flat.to_periodogram(method="bls", period=np.arange(float(period_start), float(period_end), float(period_step)))
		best_fit_period = periodogram.period_at_max_power
		lc.remove_nans().flatten(window_length=window_length).fold(period=best_fit_period, t0=periodogram.transit_time_at_max_power).bin(binsize=bin_size).plot()
		buf = io.BytesIO()
		canvas = pylab.get_current_fig_manager().canvas
		canvas.draw()
		canvas.print_png(buf)
		pylab.close()
		response = HttpResponse(buf.getvalue(), content_type='image/png')
		# Añadimos la cabecera de longitud de fichero para más estabilidad
		response['Content-Length'] = str(len(response.content))
		# Devolvemos la response
		return response
