/**
 * @description       :
 * @author            : Matias Kruk
 * @group             :
 * @last modified on  : 11-10-2020
 * @last modified by  : Matias Kruk
 * Modifications Log
 * Ver   Date         Author        Modification
 * 1.0   11-09-2020   Matias Kruk   Initial Version
 **/
import { LightningElement } from 'lwc';

const HEROKU_URL = 'https://x0planetsforce.herokuapp.com/api/';
export default class Planetsearcher extends LightningElement {
    image;
    planet = 'Kepler-93';
    quarter = 1;
    phaseImage = '';
    period_start = 0.3;
    period_end = 2;
    period_step = 0.001;
    isUploading = false;
    buildPhaseUrl() {
        this.phaseImage = HEROKU_URL + 'light_velocity/?planet=' + this.planet + '&quarter=' + this.quarter + '&period_start=' + this.period_start + '&period_end=' + this.period_end + '&period_step=' + this.period_step;
    }
    handleClick(event) {
        this.isUploading = true;
        try {
            this.buildPhaseUrl();
            this.isUploading = false;
        } catch (error) {
            this.isUploading = false;
        }
    }
    handleChangePlanet(event) {
        this.planet = event.target.value;
    }
    handleChangePhase(event) {
        this.quarter = event.target.value;
    }
    handleChangePeriodStart(event) {
        this.period_start = event.target.value;
    }
    handleChangePeriodEnd(event) {
        this.period_end = event.target.value;
    }
    handleChangePeriodStep(event) {
        this.period_step = event.target.value;
    }
    constructor() {
        super();
        this.buildPhaseUrl();
    }
}
